%
%计算关联
%
% loc1和loc2需要是一个(n,3)的数组
% Delta(j) = sqrt(2)/2 sum_{n} loc2(n, 3) c_{loc2(n, 1), u} c_{loc2(n, 2), d} 
%   - c_{loc2(n, 1), d} c_{loc2(n, 2), u}
% Delta^{d}(i) = sqrt(2)/2 sum_{n} loc1(n, 3)^{*} c^{d}_{loc1(n, 2), d} c^{d}_{loc1(n, 1), u}
%   - c^{d}_{loc1(n, 2), u} c^{d}_{loc1(n, 1), d}
% cor_ij = Delta^{d}(i) Delta(j)
function [cor_ij] = spp_corr(loc1, loc2, grn)
    size1 = size(loc1);
    size2 = size(loc2);
    val = 0.0;
    for n1 = 1:size1(1); for n2 = 1:size2(1)
        coef = 0.5 * conj(loc1(n1, 3)) * loc2(n2, 3);
        %du ud
        val = val + coef * grn(loc1(n1, 2), loc2(n2, 2)) * grn(loc1(n1, 1), loc2(n2, 1));
        %du du
        val = val + coef * grn(loc1(n1, 2), loc2(n2, 1)) * grn(loc1(n1, 1), loc2(n2, 2));
        %ud ud
        val = val + coef * grn(loc1(n1, 2), loc2(n2, 1)) * grn(loc1(n1, 1), loc2(n2, 2));
        %ud du
        val = val + coef * grn(loc1(n1, 2), loc2(n2, 2)) * grn(loc1(n1, 1), loc2(n2, 1));
    end; end
    cor_ij = val;
end

