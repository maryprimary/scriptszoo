"""生成内容"""

import os
import shutil

ptemp = """!=======================================================================================
!  Input variables for a general ALF run
!---------------------------------------------------------------------------------------

&VAR_ham_name
ham_name = "sgfr"
/

&VAR_lattice               !! Parameters defining the specific lattice and base model
L1           = {:d}            ! Length in direction a_1
L2           = 8            ! Length in direction a_2
Lattice_type = "Square"     ! Sets a_1 = (1,0), a_2=(0,1), Norb=1, N_coord=2
Model        = "Hubbard"    ! Sets the Hubbard model, to be specified in &VAR_Hubbard
/

&VAR_Model_Generic         !! Common model parameters
N_FL         = 1
N_SUN        = 1
Checkerboard = .F.          ! Whether checkerboard decomposition is used
Symm         = .F.          ! Whether symmetrization takes place
Phi_X        = 0.d0         ! Twist along the L_1 direction, in units of the flux quanta
Phi_Y        = 0.d0         ! Twist along the L_2 direction, in units of the flux quanta
Bulk         = .F.          ! Twist as a vector potential (.T.), or at the boundary (.F.)
N_Phi        = 0            ! Total number of flux quanta traversing the lattice
Dtau         = 0.1d0        ! Thereby Ltrot=Beta/dtau
Beta         = 12d0         ! Inverse temperature
Projector    = .T.          ! Whether the projective algorithm is used
Theta        = 24.d0        ! Projection parameter
N_part       = {:d}           ! Particle number
/

&VAR_QMC                   !! Variables for the QMC run
Nwrap                = 10   ! Stabilization. Green functions will be computed from
                            ! scratch after each time interval Nwrap*Dtau
NSweep               = 500   ! Number of sweeps
NBin                 = 11    ! Number of bins
Ltau                 = 0    ! 1 to calculate time-displaced Green functions; 0 otherwise
LOBS_ST              = 0    ! Start measurements at time slice LOBS_ST
LOBS_EN              = 0    ! End measurements at time slice LOBS_EN
CPU_MAX              = 0.0  ! Code stops after CPU_MAX hours, if 0 or not
                            ! specified, the code stops after Nbin bins
Propose_S0           = .F.  ! Proposes single spin flip moves with probability exp(-S0)
Global_moves         = .F.  ! Allows for global moves in space and time
N_Global             = 1    ! Number of global moves per sweep
Global_tau_moves     = .F.  ! Allows for global moves on a single time slice.
N_Global_tau         = 1    ! Number of global moves that will be carried out on a
                            ! single time slice
Nt_sequential_start  = 0    ! One can combine sequential and global moves on a time slice
Nt_sequential_end    = -1   ! The program then carries out sequential local moves in the
                            ! range [Nt_sequential_start, Nt_sequential_end] followed by
                            ! N_Global_tau global moves
Langevin            = .F.   ! Langevin update
Delta_t_Langevin_HMC=0.1    ! Default time step for Langevin and HMC updates
Max_Force           = 5.0   ! Max Force for  Langevin
HMC                 = .F.   ! HMC update
Leapfrog_steps      = 0     ! Number of leapfrog interations
/

&VAR_errors                !! Variables for analysis programs
n_skip  = 1                 ! Number of bins that to be skipped.
N_rebin = 1                 ! Rebinning
N_Cov   = 0                 ! If set to 1 covariance computed for non-equal-time
                            ! correlation functions
/

&VAR_TEMP                  !! Variables for parallel tempering
N_exchange_steps      = 6   ! Number of exchange moves
N_Tempering_frequency = 10  ! The frequency in units of sweeps at which the
                            ! exchange moves are carried out
mpi_per_parameter_set = 2   ! Number of mpi-processes per parameter set
Tempering_calc_det    = .T. ! Specifies whether the fermion weight has to be taken
                            ! into account while tempering. The default is .true.,
                            ! and it can be set to .F. if the parameters that
                            ! get varied only enter the Ising action S_0
/

&VAR_Max_Stoch             !! Variables for Stochastic Maximum entropy
Ngamma     = 400            ! Number of Dirac delta-functions for parametrization
Om_st      = -10.d0         ! Frequency range lower bound
Om_en      = 10.d0          ! Frequency range upper bound
NDis       = 2000           ! Number of boxes for histogram
Nbins      = 400            ! Number of bins for Monte Carlo
Nsweeps    = 200             ! Number of sweeps per bin
NWarm      = 200             ! The Nwarm first bins will be ommitted
N_alpha    = 14             ! Number of temperatures
alpha_st   = 1.d0           ! Smallest inverse temperature increment for inverse
R          = 1.2d0          ! temperature (see above)
Checkpoint = .F.            ! Whether to produce dump files, allowing the simulation
                            ! to be resumed later on
Tolerance  = 0.1d0          ! Data points for which the relative error exceeds the
                            ! tolerance threshold will be omitted.
/

&VAR_sgfr               !! Variables for the specific model
ham_T      = 1.d0           ! Hopping parameter
ham_chem   = 0.0d0           ! Chemical potential
ham_Tperp  = 0.d0           ! Hubbard interaction
ham_g0     = {:.6f}d0           ! For bilayer systems
ham_g1     = {:.6f}d0           ! For bilayer systems
ham_g2z    = {:.6f}d0           ! For bilayer systems
ham_g2t    = {:.6f}d0           ! For bilayer systems
/                                                                                                                  
"""


rtemp = """#!/bin/bash -f

#PBS -N LY8U{:.1f}V{:.1f}Jz{:.1f}Jt{:.1f}N{:d}
#PBS -l select=1

cd $PBS_O_WORKDIR

./ALF.out
"""


stemp = """for i in `ls`
do
  if [ -d $i ]
  then
  cd $i
  #echo `ls`
  qsub run.sh
  cd ..
  fi
done
"""


def main():
    '''entrance'''
    U = 1.6
    V = 0.0
    Jz= 7.2
    Jt= 0.4
    #
    g0 = -0.75*V -0.25*U + 0.0625*Jz + 0.125*Jt
    g1 = 0.25*V -0.25*U + 0.0625*Jz + 0.125*Jt
    g2z = -0.25*V + 0.25*U + 0.1875*Jz - 0.125*Jt
    g2t = -0.25*V + 0.25*U - 0.0625*Jz + 0.125*Jt
    print(r"g0={:.6f} g1={:.6f} g2z={:.6f} g2t={:.6f}".format(g0, g1, g2z, g2t))
    #
    frac = [0.83333]
    LX = 24
    pbase = 'LX{:d}U{:.1f}V{:.1f}Jz{:.1f}Jt{:.1f}'.format(LX, U, V, Jz, Jt)
    print(pbase)
    for den in frac:
        #lin = int(round(lin_/frac))
        part = int(round(2*LX*8*den))
        print(part)
        m4 = part%4
        if m4 == 0:
            part = [part]
        else:
            part = [part-m4, part+4-m4]
        print(part)
        for par_ in part:
            mbase = os.path.join(pbase, "N{:d}".format(par_))
            os.makedirs(mbase, exist_ok=False)
            shutil.copy("/home/matx/DQMCES/Ladder/ALF.out", os.path.join(mbase, "ALF.out"))
            shutil.copy("/home/matx/DQMCES/Ladder/ana.out", os.path.join(mbase, "ana.out"))
            shutil.copy("/home/matx/DQMCES/Ladder/seeds", os.path.join(mbase, "seeds"))
            para = open(os.path.join(mbase, "parameters"), "w")
            para.write(ptemp.format(LX, par_, -g0, -g1, -g2z, -g2t))
            para.close()
            sub = open(os.path.join(mbase, "run.sh"), "w")
            sub.write(rtemp.format(U, V, Jz, Jt, par_))
            sub.close()
        sub = open(os.path.join(pbase, "run.sh"), "w")
        sub.write(stemp)
        sub.close()


if __name__ == "__main__":
    main()

